/**
 * Created by Anton Winogradov <winogradovaa@gmail.com> on 11/27/13.
 */

$(function(){

    "use strict";

    jQuery.event.props.push('dataTransfer');

    var costs = $("#costs"),
        categories = $("#categories");

    costs

        .on("dragstart", ".a-cost", function(e){
            $(this).addClass("indrag");
            e.dataTransfer.effectAllowed = 'copy';
            e.dataTransfer.setData('id', $(this).attr("id"));
        })

        .on("dragend", ".a-cost", function(){
            $(this).removeClass("indrag");
        });

    categories

        .on("dragover", "li", function(e) {
            $(this).addClass("dragover");
            $("<span class='counter'>+</span>").insertBefore($(this).find("a"));

            if (e.preventDefault) e.preventDefault();
            return false;
        })

        .on("dragleave", "li", function() {
            $(this).removeClass("dragover");
            $(this).find(".counter").remove();
        })

        .on("drop", "li", function(e) {
            $(this).removeClass("dragover");
            $(this).find(".counter").remove();
            var id = e.dataTransfer.getData('id'),
                date = $("#" + id).data("cost-date"),
                background = $(this).css("borderLeftColor"),
                message = "\"" + $("#" + id + " .a-cost-title").text() + "\"" + " перенесено в категорию " + "\"" + $(this).find("a span").text().toLowerCase() + "\"";

            costs_storage[date][id]["category"] = $(this).attr("id");
            localStorage.setItem("costs", JSON.stringify(costs_storage));
            generate_costs(costs_storage);

            $(".a-default-message").fadeOut(200, function() {
                $(".a-sidebar__title").append(drag_message(message, background));
                $(".a-color-message").fadeIn(500, function(){
                    $(this).fadeOut(2500, function(){
                        $(".a-default-message").fadeIn(500);
                        $(".a-color-message").remove();
                    });
                });
            });
        })
});



function drag_message(message, background) {
    return "<div class='a-color-message' style='display:none;background: " + background + ";'>" + message + "</div>"
}
