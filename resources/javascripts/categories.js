/**
 * Created by Anton Winogradov <winogradovaa@gmail.com> on 11/28/13.
 */

var categories_json = {
    "category_1": {
        "color": "orange",
        "name": "Кафе и рестораны",
        "icon": "cafe"
    },
    "category_2": {
        "color": "swamp",
        "name": "Продукты питания",
        "icon": "eat"
    },
    "category_3": {
        "color": "purple",
        "name": "Транспорт",
        "icon": "transport"
    },
    "category_4": {
        "color": "green",
        "name": "Телефон",
        "icon": "phone"
    },
    "category_5": {
        "color": "blue",
        "name": "Одежда и обувь",
        "icon": "wear"
    },
    "category_6": {
        "color": "red",
        "name": "Квартира",
        "icon": "home"
    },
    "category_7": {
        "color": "gray",
        "name": "Не распределено",
        "icon": "help"
    },
    "category_8": {
        "color": "yellow",
        "name": "Компьютер",
        "icon": "computer"
    },
    "category_9": {
        "color": "brown",
        "name": "Работа",
        "icon": "none"
    },
    "category_10": {
        "color": "graphite",
        "name": "Хостинг",
        "icon": "none"
    }
};