/**
 * Created by Anton Winogradov <winogradovaa@gmail.com> on 11/28/13.
 */

$(function() {

    var toggle = false;

    $(".a-category-link").on("click", function(e) {
        show_costs_in_category($(this).parent().attr("id"), costs_storage);
        e.preventDefault();
    });

    $(".a-date-selection").on("click", function(e) {
        generate_costs(costs_storage);
        e.preventDefault();
    });

    $(".a-group-edit-link").on("click", function(e) {
        if (!toggle) {
            $(".a-cost .row .col-md-6").addClass("col-md-5").removeClass("col-md-6");
            $(".a-cost .row").prepend(group_checkbox());
            $(this).find("span").text("Отмена");
            toggle = true;
        }
        else {
            $(".a-cost .row .col-md-5").addClass("col-md-6").removeClass("col-md-5");
            $(".a-cost .row .col-md-1").remove();
            $(this).find("span").text("Групповое редактирование")
            toggle = false;
        }
        e.preventDefault();
    });

});

function group_checkbox() {
    return "<div class='col-md-1'><div class='a-group-edit'><div class='checkbox'><input type='checkbox'></div></div></div>";
}
