/**
 * Created by Anton Winogradov <winogradovaa@gmail.com> on 11/28/13.
 */

var costs_json = {
    "Чт, 31 августа": {
        "cost_1": {
            "name": "Шоколадница",
            "category": "category_1",
            "icon": "card",
            "amount": "430"
        }
    },
    "Ср, 30 августа": {
        "cost_2": {
            "name": "Pull and Bear",
            "category": "category_5",
            "icon": "card",
            "amount": "430"
        },
        "cost_3": {
            "name": "McDonalds",
            "category": "category_1",
            "icon": "card",
            "amount": "215"
        },
        "cost_4": {
            "name": "Дикси",
            "category": "category_2",
            "icon": "wallet",
            "amount": "1 280"
        }
    },
    "Вт, 31 августа": {
        "cost_5": {
            "name": "Хостинг центр",
            "category": "category_10",
            "icon": "computer",
            "amount": "200"
        }
    },
    "Пн, 28 августа": {
        "cost_6": {
            "name": "\\lux\\itunes.com\\8\\apple itus 95.00 rur",
            "category": "category_7",
            "icon": "card",
            "amount": "95"
        },
        "cost_7": {
            "name": "App Store 150.00 rur",
            "category": "category_7",
            "icon": "card",
            "amount": "150"
        },
        "cost_8": {
            "name": "Елки-Палки",
            "category": "category_1",
            "icon": "wallet",
            "amount": "1 000"
        }
    },
    "Вс, 27 августа": {
        "cost_9": {
            "name": "Bershka",
                "category": "category_5",
                "icon": "card",
                "amount": "2 645"
        },
        "cost_10": {
            "name": "ЖКХ",
                "category": "category_6",
                "icon": "card",
                "amount": "5 000"
        }
    },
    "Сб, 26 августа": {
        "cost_11": {
            "name": "МТС",
            "category": "category_4",
            "icon": "card",
            "amount": "500"
        }
    }
};