/**
 * Created by Anton Winogradov <winogradovaa@gmail.com> on 11/28/13.
 */

var costs_storage = localStorage.getItem("costs") ? JSON.parse(localStorage.getItem("costs")) : costs_json;

$(function(){

    "use strict";

    generate_categories(categories_json);
    generate_costs(costs_storage);

});


function generate_categories(categories) {
    var categories_list = $("#categories");
    $.each(categories, function(key, value){
        categories_list.append(category_item(key, value));
    });
}

function category_item(id, category) {
    return "<li class='a-sidebar-list__item a-sidebar-list__item_type_" + category["color"] +"' id='" + id + "'><a href='#' class='a-category-link'><i class='icon icon-" + category["icon"] + "'></i><span>" + category["name"] + "</span></a></li>"
}

function generate_costs(costs) {
    var costs_list = $("#costs");
    costs_list.empty();
    $.each(costs, function(key, value) {
        costs_list.append("<li class='a-main-list__item'>" + costs_by_date(key, value) + "</li>")
    });
}

function costs_by_date(date, items) {
    var costs_items = "",
        now_date;

    $.each(items, function(key, value){
        var view_date = now_date == date ? "" : date;
        costs_items += cost_item(key, value, view_date, date);
        now_date = date;
    });
    return costs_items;
}

function show_costs_in_category(category, costs) {
    var costs_list = $("#costs");
    costs_list.empty();
    $.each(costs, function(key, value) {
        costs_list.append("<li class='a-main-list__item'>" + costs_by_date_and_category(key, value, category) + "</li>")
    });

    $.each(costs_list.find(".a-main-list__item"), function(){
        if ($(this).text() == "") $(this).remove();
    });
}

function costs_by_date_and_category(date, items, category) {
    var costs_items = "",
        now_date;

    $.each(items, function(key, value){
        var view_date = now_date == date ? "" : date;
        if (value["category"] == category) {
            costs_items += cost_item(key, value, view_date, date);
        }
        now_date = date;
    });
    return costs_items;
}

function cost_item(id, cost, view_date, date) {
    var category_name = cost["category"] == "category_7" ? "Без категории (<a href='#' class='a-automove' data-toggle='modal' data-target='#autoMove'>распределить</a>)" : categories_json[cost["category"]]["name"],
        category_icon = cost["category"] == "" ? "none" : categories_json[cost["category"]]["icon"];


    return "<div class='a-cost' id='" + id + "' draggable='true' data-cost-date='" + date + "'><div class='row'>" +
                "<div class='col-md-6'>" +
                    "<div class='a-cost-about'>" +
                        "<div class='a-cost-icon'>" +
                            "<i class='icon icon-" + category_icon + "'></i>" +
                        "</div>" +
                        "<div class='a-cost-description'>" +
                            "<div class='a-cost-title'>" + cost["name"] + "</div>" +
                            "<div class='a-cost-category'>" + category_name + "</div>" +
                        "</div>" +
                    "</div>" +
                "</div>" +
                "<div class='col-md-3'>" +
                    "<div class='a-cost-type'>" +
                        "<span class='a-cost-type__from'>" +
                            "<i class='icon icon-" + cost["icon"] + "'></i>" +
                        "</span>" +
                        "<span class='a-cost-type__summ'>— " + cost["amount"] + " р.</span>" +
                    "</div>" +
                "</div>" +
                "<div class='col-md-3'>" +
                    "<div class='a-cost-date'>" + view_date + "</div>" +
                "</div>" +
            "</div>" +
        "</div>";
}