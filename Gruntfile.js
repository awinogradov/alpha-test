/*global module:false*/
module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
            '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
            '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
            '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
            ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
        settings: {
            environment: 'build',
            dirs: grunt.file.readJSON('project-dirs.json')
        },
        concat: {
            sass: {
                src: ['<%= settings.dirs.sass %>/palette.sass',
                      '<%= settings.dirs.sass %>/document.sass',
                      '<%= settings.dirs.sass %>/typography.sass',
                      '<%= settings.dirs.sass %>/icons.sass',
                      '<%= settings.dirs.sass %>/helpers.sass',
                      '<%= settings.dirs.sass %>/links.sass',
                      '<%= settings.dirs.sass %>/alerts.sass',
                      '<%= settings.dirs.sass %>/lists.sass',
                      '<%= settings.dirs.sass %>/sidebar.sass',
                      '<%= settings.dirs.sass %>/cost.sass',
                      '<%= settings.dirs.sass %>/filter.sass'],
                dest: '<%= settings.dirs[settings.environment] %>/application.sass'
            },
            external: {
                src: ['<%= settings.dirs.libs %>/bootstrap/dist/css/bootstrap.css',
                      '<%= settings.dirs[settings.environment] %>/application.css'],
                dest: '<%= settings.dirs[settings.environment] %>/application.css'
            },
            js: {
                src: ['<%= settings.dirs.libs %>/jquery/jquery.js',
                      '<%= settings.dirs.libs %>/bootstrap/dist/js/bootstrap.js',
                      '<%= settings.dirs.js %>/categories.js',
                      '<%= settings.dirs.js %>/costs.js',
                      '<%= settings.dirs.js %>/storage.js',
                      '<%= settings.dirs.js %>/dragging.js',
                      '<%= settings.dirs.js %>/sorting.js'],
                dest: '<%= settings.dirs[settings.environment] %>/application.js'
            }
        },
        uglify: {
            options: {
                banner: '<%= banner %>'
            },
            dist: {
                src: '<%= concat.dist.dest %>',
                dest: 'dist/<%= pkg.name %>.min.js'
            }
        },
        jshint: {
            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: true,
                undef: true,
                unused: true,
                boss: true,
                eqnull: true,
                globals: {
                    jQuery: true
                }
            },
            all: '<%= settings.dirs[settings.environment] %>/application.js'
        },
        sass: {
            build: {
                options: {
                    style: 'expanded'
                },
                files: {
                    '<%= settings.dirs[settings.environment] %>/application.css': '<%= settings.dirs[settings.environment] %>/application.sass'
                }
            }
        },
        watch: {
            assets: {
                files: ['Gruntfile.js', '<%= concat.sass.src %>', '<%= concat.js.src %>/*'],
                tasks: ['dist']
            }
        },
        clean: {
            build: ['<%= settings.dirs[settings.environment] %>/*.sass']
        }
    });

    // Default task.
    grunt.registerTask('default', ['jshint', 'nodeunit', 'concat', 'uglify']);
    // Task for precompile sass
    grunt.registerTask('stylesheets', ['concat:sass', 'sass:build', 'concat:external']);
    // Task for precompile js
    grunt.registerTask('javascripts', ['concat:js']);
    // Manual precompile assets
    grunt.registerTask('assets', ['stylesheets', 'javascripts']);
    // Distribute project. SET ACTUAL ENVIRONMENT IN settings.
    grunt.registerTask('dist', ['assets', 'clean:build']);
};
